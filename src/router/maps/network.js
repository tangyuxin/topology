export default [
  {
    path: 'network/linkChart',
    name: 'networkLinkChart',
    component: () => import('@/pages/network/linkChart')
  },
  {
    path: 'network/topologyChart',
    name: 'networkTopologyChart',
    component: () => import('@/pages/network/topologyChart')
  },{
    path: 'network/index',
    name: 'networkManage',
    component: () => import('@/pages/network/index')
  }
]