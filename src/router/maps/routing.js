export default [
  {
    path: 'routing/realTimeChart',
    name: 'routingRealTimeChart',
    component: () => import('@/pages/routing/realTimeChart')
  }
]